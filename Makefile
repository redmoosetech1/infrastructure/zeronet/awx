CI_REGISTRY_IMAGE=gitlab-registry.rmt:443/infrastructure/zeronet/awx
CI_COMMIT_BRANCH=main
export AWX_IMAGE_NAME=$(CI_REGISTRY_IMAGE)
export AWX_VERSION=$(CI_COMMIT_BRANCH)

.PHONY: build deploy destroy
default: build

build:
	git clone -b 17.1.0 https://github.com/ansible/awx.git awx
	docker build -t $(AWX_IMAGE_NAME):$(AWX_VERSION) -t $(AWX_IMAGE_NAME):latest awx_17.1.0/
	docker push $(AWX_IMAGE_NAME):$(AWX_VERSION)
	docker push $(AWX_IMAGE_NAME):latest
	rm -rf awx/

update:
	docker-compose up -d

deploy:
	docker-compose rm -sf
	docker-compose pull
	docker-compose up -d

destroy:
	docker-compose rm -sf
	docker container prune -f
	docker image prune -f
